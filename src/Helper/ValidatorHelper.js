/**
 * HELPER
 * Classe feit para auxiliar com validações
 */
export class ValidatorHelper {

    /** Mensagem de retorno para campos vazios */
    static EMPTY_CAMPS_MESSAGE = 'Os campos :camps não foram preenchidos!' 

    /**
     * Checa se os determinados campos em um objeto não foram preenchidos.
     * Dispara um erro caso algum não tenha sido preenchido
     * @param {*} obj objeto que será chcado
     * @param {*} campsNames nome dos campos que serão validados
     * @throws {msg: string, invalidCamps: string[]} erro que será disparado
     */
    static validateIfCampsExists(obj, campsNames = []) {
        let invalidCamps = []

        campsNames.forEach((e) => {
            if (!(obj[e])) {
                invalidCamps.push(e)
            }
        })
        
        if (invalidCamps.length > 0) {
            console.log(invalidCamps);
            throw {
                msg: ValidatorHelper.EMPTY_CAMPS_MESSAGE.replace(':camps', invalidCamps),
                invalidCamps
            }

            
        }
    }
}