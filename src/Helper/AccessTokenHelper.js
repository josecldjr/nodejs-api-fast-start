import * as config from '../config/config'
import jwt from 'jsonwebtoken'


/**
 * HELPER
 * Abstrai funcionalidades para gerenciamento de token
 */
export default class AccessTokenHelper {
    
    static secretKey = config.APP_SECRET_KEY
    static tokenExpiration = '30d'

    /**
     * Gera um token o uma informação/assinatura dentro(payload)
     * @param {*} payload informação/assinatura do token
     */
    static async generateToken (payload) { 

        var token = await jwt.sign(payload, AccessTokenHelper.secretKey, {expiresIn: AccessTokenHelper.tokenExpiration})

        return token
    }

    /**
     * Verifica e lê a informação de um token
     * @param {*} token 
     */
    static async readToken (token) {
        
       return await jwt.verify(token, this.secretKey, {})
    }

    /**
     * Extrai o token de uma requisição
     * @param {*} requisition requisição
     */
    static getBearerFromHeaders(requisition) {
        const authorizationHeader = requisition.headers['authorization']

        if (!authorizationHeader) {
            throw {message: 'Usuário não autenticado'}
        }

        let token 

        try {
            token = authorizationHeader.split(' ')[1]
        } catch (e) {
            throw {message: 'Usuário não autenticado'}
        }

        return token
    }
}