
/**
 * VALIDATOR
 * Valida se o valir fornecido é um email válido
 * @param {string} value email
 * @returns {boolean} se a string é um ermail válido ou não
 */
function validateMail (value) {
    let isValid = true

    // checa se o email inclúi um @
    if (!value.includes('@')) {
        return false
    }


    return isValid
}

/** Mensagem de retorno em caso de erro */
const message = ' is not a valid email'

export default {
    validator: validateMail,
    message: (value) => { console.log(value);
        return `${value.value} ${message}`
    }
}