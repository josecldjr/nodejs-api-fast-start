import '@babel/polyfill'
import express from 'express'
import bodyParser from 'body-parser'
import morgan from 'morgan'
import mongoose from 'mongoose'
import cors from 'cors'
import * as config from './config/config'

import routes from './routes'

// instancia da aplicação
const app = express()

/** Middlewares */
app.use(bodyParser.urlencoded({extended: true}))
app.use(bodyParser.json())
app.use(cors())

try {
    app.use((req, res, next) => {
        /** Conexão com o banco de dados */
        // const db = mongoose.connect('mongodb://localhost:27017/shares_db', {useNewUrlParser: true})
        const sqlConn = require('./connection')
        
        next()
    })
        
    /** Roteamento */
    app.use('/', routes)    
    
    /** Inicio do servidor */
    app.listen(config.PORT, (p) => {
        console.log(`App started and running on port ${config.PORT}`);        
    })

} catch (err){
    console.log(err)
    
}
