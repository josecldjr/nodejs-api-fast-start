
/**
 * ENUM
 * Contémm os tipos de publicações
 */
export default class PublicationTypesEnum {

    /** Vídeo */
    static VIDEO = 'VIDEO'
    /** Música */
    static MUSIC = 'MUSIC'

    /** Podcast  */
    static PODCAST = 'PODCAST'
    
    /** Compartilhamento */
    static SHARE = 'SHARE'

    /** Postagem normal */
    static POST = 'POST'

    /** Retorna todos os valores do enum */
    static values() {
        return [
            PublicationTypesEnum.VIDEO,
            PublicationTypesEnum.MUSIC,
            PublicationTypesEnum.PODCAST,
            PublicationTypesEnum.SHARE,
            PublicationTypesEnum.POST,
        ]
    }
}