
/**
 * UTILS
 * Utilidades para data
 */
export class DateUtils {

    /**
     * Formata a data para YYYY-MM-DD
     * @param {*} date 
     */
    static formatToYYYYMMDD(date) {        
        if (typeof(date) ===  'string') {
            date = new Date(date)
        }

        return `${date.getFullYear()}-${date.getMonth()}-${date.getDay()}`
    }
} 