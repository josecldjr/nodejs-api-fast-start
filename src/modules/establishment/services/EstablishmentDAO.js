import conn from  '../../../connection'
import {EstablishmentModel} from '../model/EstablishmentModel'
import {UserDAO} from '../../user/services/UserDAO'
import { PaymentMethodsDAO } from './PaymentMethodsDAO';

/**
 * DAO
 * Gerencia operações de banco relacionadas a lanchonetes
 */
export class EstablishmentDAO {

    userDAO
    paymentMethodsDAO

    constructor() {
        this.userDAO = new UserDAO()
        this.paymentMethodsDAO = new PaymentMethodsDAO()
    }
    
    /**
     * Cria uma lanchonete
     * @param {*} establishment lanchonete que sera criada
     */
    async save(establishment) {
        
        let sql = `
            INSERT INTO lanchonete
            (
                nome,
                telefone01,
                telefone02,
                cep,
                endereco,
                cidade,
                estado,
                id_formaPagamento,
                id_usuario,
                latitude,
                longitude                
            ) 
            VALUES
        `
            sql += '(\'' + establishment.nome + '\','
            sql += '\'' + establishment.telefone01 + '\','
            sql += '\'' + establishment.telefone02 + '\','
            sql += '\'' + establishment.cep + '\','
            sql += '\'' + establishment.endereco + '\','
            sql += '\'' + establishment.cidade + '\','
            sql += '\'' + establishment.estado + '\','
            sql +=  '\'' + establishment.id_formaPagamento + '\','
            sql +=  '\'' + establishment.id_usuario + '\','
            sql += '\'' + establishment.latitude + '\','
            sql += '\'' + establishment.longitude + '\')'
                
            return new Promise((resolve, reject) => {                
                try {
                    conn.query(sql, async (err, result) => {      
                        if (err) return reject({msg: err})     
                        
                        const establishment = await this.findById(result.insertId)

                        resolve(establishment)
                    })
                } catch (e) {
                    reject(e)
                }
            })
            
    }

    /**
     * Encontra uma lanchonete por id
     * @param {*} id id da lanchonte
     */
    async findById(id) {

        let sql = `
            SELECT * FROM lanchonete WHERE id_lanchonete = '${id}'
        `
        
        return new Promise((resolve, reject) => {
            try {
                conn.query(sql,async (err, result) => {                    
                    if (err) return reject({msg: err}) 
                    
                    const ret = result.length > 0 ? new EstablishmentModel(result[0]) : undefined

                    if (ret) {
                        ret.usuario = await this.userDAO.findById(ret.id_usuario)
                        ret.formaPagamento = await this.paymentMethodsDAO.findById(ret.id_formaPagamento)
                    }

                    resolve (ret)
                })


            } catch (err) {
                reject(err)
            }
        })
    }
}