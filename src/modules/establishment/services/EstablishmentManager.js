import { EstablishmentDAO } from "./EstablishmentDAO";
import { UserModel } from "../../user/model/UserModel";
import { PaymentMethodsDAO } from "./PaymentMethodsDAO";

/**
 * MANAGER
 * Gerencia operações relacionadas à lanchonetes
 */
export class EstablishmentManager {
    
    establishmentDAO
    paymentMethodsDAO

    constructor() {
        this.establishmentDAO = new EstablishmentDAO()
        this.paymentMethodsDAO = new PaymentMethodsDAO()
    }

    /**
     * Cria uma lanchonete
     * @param {*} establishment lanchonete que será criada
     */
    async save(establishment) {
        return await this.establishmentDAO.save(establishment)
    }

    /**
     * Lista os metodos de pagamento
     */
    async listPaymentMethods() {
        const paymentMethods = await this.paymentMethodsDAO.list()

        return {
            list: paymentMethods,
            count: paymentMethods.lenght
        }
    }

}