import conn from '../../../connection'
import { PaymentMethodModel } from '../model/PaymentMethodModel';

/**
 * DAO
 * Classe resposável por gerencia operações relacionadas a metodos de pagamento no banco
 */
export class PaymentMethodsDAO {

    /**
     * Busca um metodo de dapgamento pelo id
     * @param {*} id 
     */
    findById(id) {
        let sql = `SELECT * FROM formapagamento WHERE id_formaPagamento = ${id}`

        return new Promise((resolve, reject) => {
            conn.query(sql,async (err, result) => {
                if (err) return reject({msg:err})
                
                const ret = result.length > 0 ? new PaymentMethodModel(result[0]) : undefined

                resolve(ret)
            })
        })
    }

    /**
     * Lista os metodos de pagamento
     */
    list() {
        let sql = 'SELECT * FROM formapagamento'

        return new Promise((resolve, reject) => {
            conn.query(sql, async (err, result) => {
                if (err) return reject(err)
                console.log(result);
                
                resolve(result)
            })
        })
    }
}