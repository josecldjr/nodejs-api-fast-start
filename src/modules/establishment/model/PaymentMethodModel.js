

/**
 * MODEL
 * Model para formas de pagamento
 */
export class PaymentMethodModel {

    constructor(paymentMethod) {
        this.id_formaPagamento = paymentMethod.id_formaPagamento
        this.formaPagamento = paymentMethod.formaPagamento
    }

    id_formaPagamento
    formaPagamento
}