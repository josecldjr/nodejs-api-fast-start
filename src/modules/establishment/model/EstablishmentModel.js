
/**
 * MODEL
 * Cria uma lanchonete
 */
export class EstablishmentModel {

    constructor(establishment) {
        this.id_lanchonete = establishment.id_lanchonete
        this.nome = establishment.nome
        this.telefone01 = establishment.telefone01
        this.telefone02 = establishment.telefone02
        this.cep = establishment.cep
        this.endereco = establishment.endereco
        this.cidade = establishment.cidade
        this.estado = establishment.estado
        this.latitude = establishment.latitude
        this.longitude = establishment.longitude
        
        this.id_formaPagamento = establishment.id_formaPagamento
        this.id_usuario = establishment.id_usuario
    }

    id_lanchonete
    nome
    telefone01
    telefone02
    cep
    endereco
    cidade
    estado
    latitude
    longitude
    
    id_formaPagamento
    id_usuario

    formaPagamento = undefined
    usuario = undefined
    
}