import express from 'express'
import HttpStatus from 'http-status-codes'
import AccessTokenHelper from '../../Helper/AccessTokenHelper'
import {EstablishmentManager} from './services/EstablishmentManager'
import { EstablishmentModel } from './model/EstablishmentModel'

/*
 * CONTROLLER
 * Controller com endpoints para Establishments
 */

const router = express.Router()
 
const establishmentManager =  new EstablishmentManager()

/**
 * Cria uma lanchonete
 */
router.post('/', async (req, res) => {

    const establishment = req.body

    try {
        const createdEstablishment = await establishmentManager.save(establishment)

        res.status(HttpStatus.CREATED).json(createdEstablishment)

    } catch (err) {
        console.log(err)        
        res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({msg:err})
    }

})

/**
 * Retorna as formas de pagamento deisponíveis
 */
router.get('/payment-methods', async (req, res) => {
    try {
        const paymentMethods = await establishmentManager.listPaymentMethods()

        res.status(HttpStatus.OK).json(paymentMethods)
    } catch (err) {
        console.log(err)
        res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({msg:err})        
    }
})


module.exports = router