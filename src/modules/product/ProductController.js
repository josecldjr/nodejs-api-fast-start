import express from 'express'
import HttpStatus from 'http-status-codes'
import { ProductModel } from './model/ProductModel';
import { ProductManager } from './services/ProductManager';
import { ValidatorHelper } from '../../Helper/ValidatorHelper';

/*
 * CONTROLLER
 * Controller com endpoints para Establishments
 */

const router = express.Router()

const productManager = new ProductManager()

/**
 * POST
 * Cria um produto e retorna o que foi criado
 */
router.post('/', async (req, res) => {

    const product = new ProductModel(req.body)

    try {
        ValidatorHelper.validateIfCampsExists(product, ['nome','descricao','id_lanchonete'])

        const retProduct = await productManager.save(product)

        res.status(HttpStatus.CREATED).json(retProduct)
    } catch (err) {        
        res.status(HttpStatus.INTERNAL_SERVER_ERROR).json(err)
    }
})


module.exports = router