/**
 * MODEL
 * Model para produto
 */
export class ProductModel {
    id_produto
    nome
    descricao
    preco

    id_categoria
    id_lanchonete

    categoria
    lanchonete

    constructor(product) {
        this.id_produto = product.id_produto 
        this.nome = product.nome 
        this.descricao = product.descricao 
        this.preco = product.preco 

        this.id_categoria = product.id_categoria
        this.id_lanchonete = product.id_lanchonete

        
    }
}