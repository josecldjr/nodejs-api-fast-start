import { ProductDAO } from './ProductDAO'

/**
 * MANAGER
 * Responsável por gerenciar operações relacionadas a produto
 */
export class ProductManager {

    productDAO

    constructor() {
        this.productDAO = new ProductDAO()
    }

    /**
     * Cria um produto 
     * @param {*} product dados/model do produto
     */
    async save(product) {
        return await this.productDAO.save(product)
    }

    /**
     * Retorna um produto pelo id
     */
    async findById(id, relations) {
        return await this.productDAO.findById(id, relations)
    }
}