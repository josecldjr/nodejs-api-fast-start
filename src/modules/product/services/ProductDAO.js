import conn from '../../../connection'
import { ProductModel } from '../model/ProductModel'
import {EstablishmentDAO} from '../../establishment/services/EstablishmentDAO'

/**
 * DAO
 * Gerencia operações no banco de dados relacionadas a produto
 */
export class ProductDAO {

    establishmentDAO

    constructor() {
        this.establishmentDAO = new EstablishmentDAO()
    }

    /**
     * Cria um produto no banco de dados
     * @param {*} product 
     */
    async save (product) {
        let sql = `
            INSERT INTO produto
            (
                nome,
                descricao,
                preco,
                id_categoria,
                id_lanchonete
            ) VALUES         
        ` 

        sql += '(\'' + product.nome + '\','
        sql += '\'' + product.descricao + '\','
        sql += '\'' + product.preco + '\','
        sql += '\'' + product.id_categoria + '\','
        sql += '\'' + product.id_lanchonete + '\')'
 
        return new Promise( async (resolve, reject) => {

            try {
                conn.query(sql, async (err, result) => {
                    if (err) return reject(err)
                    
                    let ret = await this.findById(result.insertId)
                    
                    resolve(ret)
                })

            } catch (e) {
                reject(e)
            }

        })
        
    }

    /**
     * Encontra um produto pelo id
     * @param {number} id id do produto
     * @param {boolean} relations popular relações?
     */
    async findById(id, relations = true) {
        let sql = `SELECT * FROM produto WHERE id_produto = ${id}`
    
        return new Promise((resolve, reject) => {

            try {

                conn.query(sql, async (err, result) => {
                    if (err) return reject(err)

                    let ret = result.length > 0 ? new ProductModel(result[0]) : undefined
 
                    if (ret) {
                        ret.lanchonete = relations ? await this.establishmentDAO.findById(ret.id_lanchonete) : undefined
                    
                    }

                    resolve(ret)
                })

            } catch (e) {
                reject(e)
            }

        })
    
    }
}