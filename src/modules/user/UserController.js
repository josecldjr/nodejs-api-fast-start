import { UserManager } from './services/UserManager';
import express from 'express'
import HttpStatus from 'http-status-codes'
import { UserFormatter } from './services/UserFormatter';
import AccessTokenHelper from '../../Helper/AccessTokenHelper';
import {EstablishmentManager} from '../establishment/services/EstablishmentManager'
import verifyUser from '../../middlewares/VerifyUser';

/*
 * CONTROLLER
 * Controller com endpoints para User
 */

const router = express.Router()

const userManager = new UserManager()
const establishmentManager = new EstablishmentManager()
/* Cria um usuário */
router.post('/', async (req, res) => {
    const user = req.body
    
    try {        
        
        const ret  = await userManager.save(user)

        res.status(HttpStatus.CREATED).json(ret)
    } catch (err) {
        res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({err})
    }
})

/* Faz o login */
router.post('/login', async (req, res) => {
    let ret = {}
    let token
    
    if (!req.body.email || !req.body.senha) {
        res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({msg: 'Email se senha devem ser preenchidos'})
    }

    try {        
        ret = await userManager.findByEmailAndPassword(req.body)
        
        if (!ret) return res.status(HttpStatus.NOT_FOUND).json({msg: 'Usuário ou senha incorretos'})
        
        token = await AccessTokenHelper.generateToken({id: ret.id_usuario})
        
        res.json({
            accessToken: token,
            user: UserFormatter.formatUserForLoginReturn(ret)
        })
    } catch (e) {
        console.log('err',e)
        res.status(HttpStatus.INTERNAL_SERVER_ERROR).json(e)

    }
    
})

/**
 * Cria uma lanchonete como usuário
 */
router.post('/create-establishment', verifyUser, async (req, res) => {

    const establishment = req.body

    try {
        establishment.id_usuario = req.user.id_usuario

        const ret = await establishmentManager.save(establishment)

        res.status(HttpStatus.CREATED).json(ret)
    } catch (err) {
        console.log(err)        
        res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({err})
    }

})

export default router