import { UserDAO } from './UserDAO';
import { UserModel } from './../model/UserModel';

/**
 * MANAGER
 * Manager para user
 */
export class UserManager {

    userDao

    constructor() {
        this.userDao = new UserDAO()
    }

    /**
     * Cria um usuário
     * @param {*} user 
     */
    async save(user) {
      
      if (!user.senha) {
        throw({msg: 'A senha deve ser preenchida'})
      }

      if (!user.nome) {
        throw({msg: 'O nome deve ser preenchido'})
      }

      if (!user.email) {
        throw({msg: 'O email deve ser preenchido'})
      }

      return await this.userDao.save(user)      
    }

    /**
     * Retorna um usuário pelo id
     * @param {*} id 
     */
    async findById(id) {        
      return await this.userDao.findById(id)
    }
    /**
     * Encontra um usuário pelo email e senha
     * @param {*} filters 
     */
    async findByEmailAndPassword(filters) { 

      const loginFilter = {
        email: filters.email,
        senha: filters.senha
      }
      
      return await this.userDao.findOneByFilters(loginFilter)
    }

 
}