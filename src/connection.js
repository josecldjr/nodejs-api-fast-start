const mysql = require('mysql')
const dbConfig = require('./config/database')

const connection = mysql.createConnection(
    process.env.DATABASE_URL || 
    process.env.CLEARDB_DATABASE_URL  ||
    dbConfig
) 

// connection e callback com mesagem de sucesso
connection.connect((err) => {
    if (err) return console.log('err', err)

    console.log('Database connected successfully!')        
})

connection.on('error', function(err) {
    console.log('db error', err)

    if(err.code === 'PROTOCOL_CONNECTION_LOST') {
        setTimeout(() => {
            connection.connect((err) => {
                if (err) return console.log('err', err)
            
                console.log('Database RE-connected successfully!')        
            }) 
        }, 3000)                   
    } else {                                    
        throw err;
    }
});

module.exports = connection